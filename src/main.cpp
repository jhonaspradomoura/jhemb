#include "Arduino.h"


static bool     bLdrActive        = true;
static uint8_t  ui8BrilhoLED      = 0;



void vUser(String read)
{
    if (strcmp(&read[0], "d") == 0 || strcmp(&read[0], "D") == 0)
    {
        bLdrActive = !bLdrActive;
        Serial.print("LDR esta ");
        Serial.println(bLdrActive ? "ativado" : "desativado");
    }
    else
    {
        char *pEnd;
        ui8BrilhoLED = strtol(read.c_str(), &pEnd, 10);
        Serial.print("Mudando brilho para ");
        Serial.println(ui8BrilhoLED);

    }
}

void setup()
{
    Serial.begin(9600);
    pinMode(12, INPUT_PULLUP);
    pinMode(10, OUTPUT);
}


void loop()
{
    static uint16_t ui16LDR = 0;
    static String   read;
    static bool bLedState = true;
    ui16LDR = analogRead(A0);


    if (bLdrActive)
    {
        ui8BrilhoLED = ui16LDR / 6;
    }
    if (Serial.available() > 0)
    {
        read = Serial.readString();
        read.trim();
        vUser(read);
    }
    if (!digitalRead(12))
    {
        bLedState = !bLedState;
        Serial.print("Botao pressionado! Estado novo = ");
        Serial.println(bLedState);
        delay(200);
    }

    if (bLedState)
    {
        analogWrite(10, ui8BrilhoLED);
    }
    else
    {
        analogWrite(10, 0);
    }
}
